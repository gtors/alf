package gtors.health;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    @GetMapping("/admin/health")
    Status healthCheck() {
        return new Status();
    }

    @Data
    @NoArgsConstructor
    public static class Status {
        private String status = "UP";
    }
}