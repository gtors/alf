package gtors;


import lombok.Data;

@Data
public class ErrorResponse {

    private String status;
}