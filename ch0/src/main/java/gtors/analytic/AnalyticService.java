package gtors.analytic;

import com.fasterxml.jackson.databind.ObjectMapper;
import gtors.analytic.domain.UserPaymentAnalytic;
import gtors.analytic.domain.UserPaymentStats;
import gtors.analytic.domain.UserPaymentTemplate;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class AnalyticService {
    final private ConcurrentHashMap<String, UserPaymentAnalytic> analytics = new ConcurrentHashMap<>();
    final private ConcurrentHashMap<String, UserPaymentStats> stats = new ConcurrentHashMap<>();
    final private ConcurrentHashMap<String, List<UserPaymentTemplate>> templates = new ConcurrentHashMap<>();

    public Optional<UserPaymentAnalytic> getConcreteUserAnalytic(String userId) {
        return Optional.ofNullable(analytics.get(userId));
    }

    public Optional<UserPaymentStats> getConcreteUserStats(String userId) {
        return Optional.ofNullable(stats.get(userId));
    }
    
    public List<UserPaymentAnalytic> getAllAnalytic() {
        return new ArrayList<>(analytics.values());
    }
    
    public Optional<List<UserPaymentTemplate>> getConcreteUserTemplates(String userId) {
        return Optional.ofNullable(templates.get(userId));
    }

    @KafkaListener(topics = "RAW_PAYMENTS")
    public void consume(@Payload KafkaMessage msg) {
        System.out.println(msg);
        System.out.println(msg.isProcessable());
        if (msg != null && msg.isProcessable()) {
            System.out.println("Process");

//            var userStats = stats.computeIfAbsent(msg.userID, k -> UserPaymentStats.builder()
//                    .maxAmountCategoryID(BigDecimal.ZERO)
//                    .minAmountCategoryID(BigDecimal.ZERO)
//                    .build()
//            );
            populateAnalytics(msg);
        }
    }

    private void populateStats(KafkaMessage msg) {
        var st = stats.computeIfAbsent(
                msg.userId,
                k -> new UserPaymentStats());

        var amount = BigDecimal.valueOf(msg.getAmount());
    }

    private void populateAnalytics(KafkaMessage msg) {
        var an = analytics.computeIfAbsent(
                msg.userId,
                k -> new UserPaymentAnalytic(msg.userId));

        var aggr = an.getAnalyticInfo().computeIfAbsent(
                msg.categoryId,
                k -> new UserPaymentAnalytic.Aggregation());
        
        var amount = BigDecimal.valueOf(msg.getAmount());
        
        aggr.setSum(aggr.getSum().add(amount));
        aggr.setMax(aggr.getMax().max(amount));

        if (aggr.getMin().equals(BigDecimal.ZERO)) {
            aggr.setMin(amount);
        } else{
            aggr.setMin(aggr.getMin().min(amount));
        }

        an.setTotalSum(an.getTotalSum().add(amount));
    }

    @Data
    @NoArgsConstructor
    public static class KafkaMessage {
        private String ref;
        private Long categoryId;
        private String userId;
        private String recipientId;
        private String desc;
        private Double amount;

        boolean isProcessable() {
            return categoryId != null && userId != null && amount != null;
        }
    }
}
