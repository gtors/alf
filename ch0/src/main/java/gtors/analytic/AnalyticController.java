package gtors.analytic;

import gtors.analytic.domain.UserPaymentAnalytic;
import gtors.analytic.domain.UserPaymentStats;
import gtors.analytic.domain.UserPaymentTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class AnalyticController {
    final private AnalyticService analyticService;
    
    public AnalyticController(AnalyticService analyticService) {
        this.analyticService = analyticService;
    }

    @GetMapping("/analytic")
    List<UserPaymentAnalytic> all() {
        return analyticService.getAllAnalytic();
    }

    @GetMapping("/analytic/{userId}")
    UserPaymentAnalytic getConcreteUserAnalytic(@PathVariable("userId") String userId) {
        return analyticService.getConcreteUserAnalytic(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/analytic/{userId}/stats")
    UserPaymentStats getConcreteUserStats(@PathVariable("userId") String userId) {
        return analyticService.getConcreteUserStats(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/analytic/{userId}/templates")
    List<UserPaymentTemplate> getConcreteUserTemplates(@PathVariable("userId") String userId) {
        return analyticService.getConcreteUserTemplates(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
