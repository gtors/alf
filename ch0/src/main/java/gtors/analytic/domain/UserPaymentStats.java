package gtors.analytic.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;


@Data
public class UserPaymentStats {
    private BigDecimal maxAmountCategoryID = BigDecimal.ZERO;
    private BigDecimal minAmountCategoryID = BigDecimal.ZERO;
    private Long oftenCategoryID;
    private Long rareCategoryID;
}

