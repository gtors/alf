package gtors.analytic.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashMap;

@Data
public class UserPaymentAnalytic {
    private HashMap<Long, Aggregation> analyticInfo = new HashMap<>();
    private BigDecimal totalSum = BigDecimal.ZERO;
    private String userID;

    public UserPaymentAnalytic(String userID) {
        this.userID = userID;
    }

    @Data
    @NoArgsConstructor
    public static class Aggregation {
        private BigDecimal max = BigDecimal.ZERO;
        private BigDecimal min = BigDecimal.ZERO;
        private BigDecimal sum = BigDecimal.ZERO;
    }
}





