package gtors.analytic.domain;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserPaymentTemplate {
    private BigDecimal amount;
    private Long categoryID;
    private String recipientID;
}
